<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIsvIsvFilterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('isv_isv_filter', function(Blueprint $table)
        {
            $table->integer('isv_id')->unsigned();
            $table->integer('isv_filter_id')->unsigned();
            $table->foreign('isv_id')->references('id')->on('isvs')->onDelete('cascade');
            $table->foreign('isv_filter_id')->references('id')->on('isv_filters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('isv_isv_filter');
    }
}
