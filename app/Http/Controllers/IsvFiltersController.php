<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Post;
use App\Expo;
use App\Isv;
use App\IsvFilter;

class IsvFiltersController extends Controller
{

    /**
     * Show all apps
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Filters
        $filters = IsvFilter::all();

        $regionFilter = $filters->filter(function ($item) {
            return $item->filter_type == IsvFilter::filterTypes()[0];
        })->values();
        $segmentFilter = $filters->filter(function ($item) {
            return $item->filter_type == IsvFilter::filterTypes()[1];
        })->values();
        $categoryFilter = $filters->filter(function ($item) {
            return $item->filter_type == IsvFilter::filterTypes()[2];
        })->values();
        $purposeFilter = $filters->filter(function ($item) {
            return $item->filter_type == IsvFilter::filterTypes()[3];
        })->values();

        return view('isvfilters.index', array(
            'regionFilter' => $regionFilter,
            'segmentFilter' => $segmentFilter,
            'categoryFilter' => $categoryFilter,
            'purposeFilter' => $purposeFilter,
        ));
    }
}
