<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Post;
use App\Expo;
use App\Isv;
use App\IsvFilter;
use Illuminate\Support\Facades\Input;

class PagesController extends Controller
{

    /**
     * Show home
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->show('home');
    }

    /**
     * Live edit
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($slug) {
        if (\Auth::user()) {
            if (\Auth::user()->can('pages')) {
                $page = Page::where('slug', $slug)->firstOrFail();
                return view('pages.edit', array(
                    'page' => $page,
                ));
            }
            \App::abort(404);
        }
        return redirect('/admin');
    }

    /**
     * Live edit update
     *
     * @return \Illuminate\Http\Response
     */
    public function update($slug) {
        if (\Auth::user()->can('pages')) {
            $page = Page::where('slug', $slug)->firstOrFail();
            $page->content = Input::get('content');
            if ($page->save()) {
                return \Response::json([
                    'message'=>'Page updated',
                    'message_type' => 'success'
                ]);
            }
            return \Response::json([
                'message'=>'Error updating',
                'message_type' => 'error'
            ]);
        }
        \App::abort(404);
    }

    /**
     * Show page by slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        // Posts and expos
        $posts = Post::where('status', 1)->take(10)->get();
        $expos = Expo::where('status', 1)->take(10)->get();
        $page = Page::where('slug', $slug)->where('status', '1')->firstOrFail();
        return view('pages.show', array(
            'page' => $page,
            'posts' => $posts,
            'expos' => $expos,
        ))->withShortcodes();
    }
}
