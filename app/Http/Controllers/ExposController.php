<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expo;

class ExposController extends Controller
{

    /**
     * Show post by slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $expo = Expo::where('slug', $slug)->where('status', '1')->firstOrFail();
        return view('expos.show', array(
            'expo' => $expo,
        ));
    }
}
