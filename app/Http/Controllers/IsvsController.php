<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Page;
use App\Post;
use App\Expo;
use App\Isv;
use App\IsvFilter;

class IsvsController extends Controller
{

    /**
     * Show all apps
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $isvs = Isv::where('status', '1')->orderBy('name', 'asc')->with('filters');

        // Filter by input
        if (Input::get('region')) {
            $isvs->whereHas('filters', function($query) {
                $query->where('id', Input::get('region'));
                $query->where('filter_type', 0);
            });
        }
        if (Input::get('segment')) {
            $isvs->whereHas('filters', function($query) {
                $query->where('id', Input::get('segment'));
                $query->where('filter_type', 1);
            });
        }
        if (Input::get('category')) {
            $isvs->whereHas('filters', function($query) {
                $query->where('id', Input::get('category'));
                $query->where('filter_type', 2);
            });
        }
        if (Input::get('purpose')) {
            $isvs->whereHas('filters', function($query) {
                $query->where('id', Input::get('purpose'));
                $query->where('filter_type', 3);
            });
        }

        $isvs = $isvs->paginate(10);

        if (\Request::ajax()) {
            return view('isvs.items', array(
                'isvs' => $isvs,
            ));
        }

        return view('isvs.index', array(
            'isvs' => $isvs,
        ));
    }

    /**
     * Show lightbox app
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $isv = Isv::where('id', $id)->where('status', '1')->with('filters')->firstOrFail();
        return view('isvs.show', array(
            'isv' => $isv,
        ));
    }
}
