<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{

    /**
     * Show post by slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->where('status', '1')->firstOrFail();
        return view('posts.show', array(
            'post' => $post,
        ));
    }
}
