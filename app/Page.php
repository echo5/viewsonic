<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    /* Get enum label
     *
     * @param $value
     * @return str
     */
    public function getStatusAttribute($value)
    {
        if (is_int($value)) {
            return $this->statuses()[$value];
        }
    }

    /**
     * Statuses
     *
     * @return array
     */
    public static function statuses()
    {
    	return [
    		0 => 'Inactive',
    		1 => 'Active',
    	];
    }
}
