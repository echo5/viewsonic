<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Post extends Model
{
    /**
     * Set user to current if not set
     *
     * @param  string  $value
     */
    public function setUserIdAttribute($value)
    {
        if (!$value) {
        	$this->attributes['user_id'] = Auth::id();
        }
    }

    /**
     * Get enum label
     *
     * @param $value
     * @return str
     */
    public function getStatusAttribute($value)
    {
        if (is_int($value)) {
            return $this->statuses()[$value];
        }
    }

    /**
     * Statuses
     *
     * @return array
     */
    public static function statuses()
    {
        return [
            0 => 'Draft',
            1 => 'Published',
        ];
    }
}
