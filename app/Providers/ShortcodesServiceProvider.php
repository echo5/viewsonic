<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ShortcodesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        \Shortcode::register('isvs', 'App\Http\Controllers\IsvsController@index');
        \Shortcode::register('isv_filters', 'App\Http\Controllers\IsvFiltersController@index');
    }
}
