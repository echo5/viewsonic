<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IsvFilter extends Model
{
	/**
	 * Filter types
	 *
	 * @return array
	 */
	public static function filterTypes()
	{
		return [
			0 => 'Region',
			1 => 'Segment',
			2 => 'Category',
			3 => 'Purpose',
		];
	}

	/**
	 * Get filter type
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getFilterTypeAttribute($value)
	{
		if (is_int($value)) {
		    return $this->filterTypes()[$value];
		}
	}

	public function regions()
	{
		return $this->filter(function ($item) {
		    return $item->filter_type == $this->filterTypes()[2];
		})->values();
	}
}
