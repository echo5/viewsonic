<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expo extends Model
{
    /**
     * Statuses
     *
     * @return array
     */
    public static function statuses()
    {
        return [
            0 => 'Draft',
            1 => 'Published',
        ];
    }

    /**
     * Get enum label
     *
     * @param $value
     * @return str
     */
    public function getStatusAttribute($value)
    {
        if (is_int($value)) {
            return $this->statuses()[$value];
        }
    }
}
