<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\IsvFilter;

class Isv extends Model
{

	/**
	 * Filters relationship
	 */
    public function filters() 
    {
    	return $this->belongsToMany('App\IsvFilter', 'isv_isv_filter', 'isv_id', 'isv_filter_id');
    }

    /**
     * Filters relationship
     */
    public function regions() 
    {
        return $this->belongsToMany('App\IsvFilter', 'isv_isv_filter', 'isv_id', 'isv_filter_id')->where('isv_filters.filter_type', 0);
    }

    /**
     * Filters relationship
     */
    public function categories() 
    {
        return $this->belongsToMany('App\IsvFilter', 'isv_isv_filter', 'isv_id', 'isv_filter_id')->where('isv_filters.filter_type', 2);
    }

    public function getCategoriesAttribute($value)
    {
        return $this->filters->filter(function ($item) {
            return $item->filter_type == IsvFilter::filterTypes()[2];
        })->values();
    }

    /**
     * Image 1
     */
    public function getImage1Attribute($value)
    {
        return $this->getCropped($value);
    }

    /**
     * Image 2
     */
    public function getImage2Attribute($value)
    {
        return $this->getCropped($value);
    }

    /**
     * Image 3
     */
    public function getImage3Attribute($value)
    {
        return $this->getCropped($value);
    }

    /**
     * Return cropped filename
     */
    public function getCropped($value)
    {
        $ext = pathinfo($value, PATHINFO_EXTENSION);
        $filename = str_replace('.'.$ext, '', $value). '-cropped.' .$ext;
        return ($filename);
    }

	/**
	 * Author relationship
	 */
    public function author() 
    {
    	return $this->belongsTo('App\User');
    }

    /**
     * Video types
     *
     * @return array
     */
    public static function videoTypes()
    {
        return [
            0 => 'Youtube',
            // 1 => 'Vimeo',
            2 => 'MP4',
        ];
    }

    public function video()
    {
        switch ($this->video_type) {
            case 0:
                return preg_replace(
                    "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
                    "<iframe src=\"//www.youtube.com/embed/$2\" allowfullscreen id=\"isv-video\"></iframe>",
                    $this->video_url
                );
                break;
            case 1:
                return '<iframe src="' . $this->video_url .'"></iframe>';
                break;
            case 2:
                return '<video width="320" height="240" controls id="isv-video">
                  <source src="'. $this->video_url .'" type="video/mp4">
                Your browser does not support this video type.
                </video>';
                break;
        }
    }

    /**
     * Get enum label
     *
     * @param $value
     * @return str
     */
    public function getStatusAttribute($value)
    {
        if (is_int($value)) {
            return $this->statuses()[$value];
        }
    }

    /**
     * Statuses
     *
     * @return array
     */
    public static function statuses()
    {
        return [
            0 => 'Pending',
            1 => 'Approved',
            2 => 'Denied',
        ];
    }

    /**
     * Extra fields used in admin
     */
    public static function adminFields()
    {
        return [
            [
                'Field' => 'filters',
                'Type' => 'multiple_select',
                'Null' => 'YES',
                'Key' => '',
                'Default' => null,
                'Extra' => '',
            ],
            [
                'Field' => 'regions',
                'Type' => 'multiple_select',
                'Null' => 'YES',
                'Key' => '',
                'Default' => null,
                'Extra' => '',
            ]
        ];
    }
}
