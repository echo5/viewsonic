<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Cache\TaggableStore;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use InvalidArgumentException;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Extra fields used in admin
     */
    public static function adminFields()
    {
        return [
            [
                'Field' => 'roles',
                'Type' => 'multiple_select',
                'Null' => 'YES',
                'Key' => '',
                'Default' => null,
                'Extra' => '',
            ]
        ];
    }

    /**
     * Return value for delete
     */
    public function delete(array $options = [])
    {   //soft or hard
        if(!parent::delete($options)) {
            return false;
        }
        if(Cache::getStore() instanceof TaggableStore) {
            Cache::tags(Config::get('entrust.role_user_table'))->flush();
        }
        return true;
    }
}
