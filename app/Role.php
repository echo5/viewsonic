<?php namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{

	/**
	 * Filters relationship
	 */
    public function permissions() 
    {
    	return $this->belongsToMany('App\Permission');
    }

	/**
	 * Extra fields used in admin
	 */
	public static function adminFields()
	{
	    return [
	        [
	            'Field' => 'permissions',
	            'Type' => 'multiple_select',
	            'Null' => 'YES',
	            'Key' => '',
	            'Default' => null,
	            'Extra' => '',
	        ]
	    ];
	}

}