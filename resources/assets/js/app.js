
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('select2');
require('lightslider');

/**
 * Fn to allow an event to fire after all images are loaded
 */
$.fn.imagesLoaded = function () {

    // get all the images (excluding those with no src attribute)
    var $imgs = this.find('img[src!=""]');
    // if there's no images, just return an already resolved promise
    if (!$imgs.length) {return $.Deferred().resolve().promise();}

    // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
    var dfds = [];  
    $imgs.each(function(){

        var dfd = $.Deferred();
        dfds.push(dfd);
        var img = new Image();
        img.onload = function(){dfd.resolve();}
        img.onerror = function(){dfd.resolve();}
        img.src = this.src;

    });

    // return a master promise object which will resolve when all the deferred objects have resolved
    // IE - when all the images are loaded
    return $.when.apply($,dfds);

}

$(document).ready(function() {
	/**
	 * Select2 boxes
	 */
	$(".filter select").select2({
		minimumResultsForSearch: Infinity
	});

	/**
	 * Footer slide open
	 */
	$('#footer-toggle').click(function() {
		$('#footer').toggleClass('active');
	});


	/**
	 * Infinite scroll for apps
	 */
	var $isvContainer = $('#isv'),
		  $isvPagination = $('#isv-pagination'),
		  $isvLoading = $('#isv-loading');

	var page = 1;
	var total = 10;
	var allowLoading = true;
	var isvsUrl = '/isvs?';
	$isvPagination.hide();

	/**
	 * Load new apps
	 */
	function getApps(page, replace) {
		$isvLoading.css({'visibility': 'visible'});
		url = isvsUrl + '&page=' + page;
		$.ajax({
			url : url,
			// async: false,
		}).done(function (data) {
			$isvLoading.css({'visibility': 'hidden'});
			if (replace == true) {
				$('#isvs').html(data);
			} else {
				$('#isvs').append(data);
			}
			if (!data.trim() == '') {
		  		allowLoading = true;
			}
		}).fail(function () {
			alert('Apps could not be loaded.');
		});
	}

	/**
	 * Get new apps when scrolled to pagination area
	 */
	if ($isvPagination.length) {
		$(window).scroll(function () {
			if ($(window).scrollTop() >= ($('#isv-pagination').offset().top - $(window).height())) {
				if (!allowLoading) {
					return false;
				} else {
					allowLoading = false;
					page++;
					getApps(page);
				}
			}
		});
	}

	/**
	 * Update URL and apps on filter change
	 */
	var selectFilters = $('.filter select');
	selectFilters.on('change', function() {
		isvsUrl = '/isvs?';
		selectFilters.each(function() {
			var key = $(this).attr('name');
			var value = $(this).val();
			if (value != '') {
				isvsUrl = isvsUrl + key + '=' + value + '&';
			}
		});
		// Remove trailing ampersand &
		if (isvsUrl.substring(isvsUrl.length - 1) == '&') {
			isvsUrl = isvsUrl.substring(0, isvsUrl.length - 1);
		}
		page = 1;
		getApps(page, true);
	});

	/**
	 * Load apps via ajax into modal
	 */
	var modal = $('#isv-modal');
	$(document).on('click', '.isv-link', function(e) {
		e.preventDefault();
	    modal.find('.modal-content').html('<div class="loading"><i class="ion-ios-sync"></i></div>');
		modal.modal('show');
	    var url = $(this).attr('href');
	    $.ajax({
	    	url : url,
	    }).done(function (data) {
	    	modal.find('.modal-content').append(data).imagesLoaded().then(function() {
		    	startSlider();
		    	$(window).resize();
		    	modal.find('.loading').remove();
	    	});
	    });
	});

	/**
	 * Slider for apps
	 */
	function startSlider() {
		$("#modal-slider").lightSlider({
			item: 1,
			slideMargin: 0,
			onSliderLoad: function() {
			    $('#modal-slider').removeClass('cS-hidden');
		    }
		});
	}

	/**
	 * Start video for ISV
	 */
	var body = $('body');
	function startVideo() {
		var video = $('#isv-video');
		var videoType = body.data('video-type');

		body.addClass('video-active');
		switch (videoType) {
			case 0:
				video.attr('src', video.attr('src') + '?autoplay=1');
				break;
			case 1:
				break;
			case 2:
				video.get(0).play();
				break;
		}
	}
	$(document).on('click', '.video-play-btn', function(e) {
		e.preventDefault();
		body.data('video-type', $(this).data('video-type'));
		startVideo();
	});

	/**
	 * Start video for ISV
	 */
	function stopVideo() {
		var video = $('#isv-video');
		var videoType = body.data('video-type');

		body.removeClass('video-active');
		switch (videoType) {
			case 0:
				video.attr('src', video.attr('src').replace('?autoplay=1', ''));
				break;
			case 1:
				break;
			case 2:
				video.get(0).pause();
				break;
		}
	}
	$(document).on('click', '.video-stop-btn', function(e) {
		e.preventDefault();
		body.data('video-type', $(this).data('video-type'));
		stopVideo();
	});

	$('#isv-modal').on('hidden.bs.modal', function () {
	    stopVideo();
	})

	/**
	 * Open footer links in lightbox
	 */
	 var postModal = $('#post-modal');
	 $('.modal-link').on('click', function(e) {
	 		e.preventDefault();
	 	    postModal.find('.modal-content').html('<div class="loading"><i class="ion-ios-sync"></i></div>');
	 		postModal.modal('show');
	 	    var url = $(this).attr('href');
	 	    $.ajax({
	 	    	url : url,
	 	    }).done(function (data) {
	 	    	postModal.find('.modal-content').append(data).imagesLoaded().then(function() {
	 		    	postModal.find('.loading').remove();
	 	    	});
	 	    });
	 });

	 $('.modal-close').on('click', function(e) {
	 		e.preventDefault();
	 	    modal = $(this).closest('.modal');
	 	    console.log(modal);
	 		modal.modal('hide');
	 });

});




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });
