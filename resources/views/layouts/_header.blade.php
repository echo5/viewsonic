        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ Voyager::image( Voyager::setting('logo') ) }}" alt="{{ Voyager::setting('title') }}">
                        {{ Voyager::setting('description') }}
                    </a>
                </div>

            </div>
        </nav>