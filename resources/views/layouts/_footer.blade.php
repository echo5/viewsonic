<!-- Footer -->
<div class="footer" id="footer">
    <a class="footer-toggle" id="footer-toggle">
        <i class="ion ion-ios-arrow-up" id="footer-toggle-icon"></i>
    </a>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4>{{ Voyager::setting('footer_column_1') }}</h4>
                <ul class="latest-posts list-unstyled">
                    @if (count($posts))
                        @foreach ($posts as $post)
                            <li><a class="modal-link" href="/news/{{ $post->slug }}">{{ $post->title }}</a></li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="col-md-4">
                <h4>{{ Voyager::setting('footer_column_2') }}</h4>
                <ul class="latest-expos list-unstyled">
                @if (count($expos))
                    @foreach ($expos as $expo)
                        <li><a class="modal-link" href="/expos/{{ $expo->slug }}">{{ $expo->name }}</a> on {{ Carbon\Carbon::parse($expo->start_date)->format('M d, Y') }}</li>
                    @endforeach
                @endif
                </ul>
            </div>
            <div class="col-md-4">
                {!! Voyager::setting('footer_box') !!}
            </div>
        </div>
    </div>
    <div class="footer-copy text-center">
        Copyright © <?php echo date("Y"); ?> ViewSonic.<br/>
    </div>
</div>
<div class="footer-copy text-center">
    <span class="designed-by">Designed by <a href="http://echo5web.com" target="_blank">Echo 5 - Atlanta Web Design</a></span>
</div>

<!-- Post Modal -->
<div class="modal fade" id="post-modal" tabindex="-1" role="dialog" aria-labelledby="post-modal" aria-hidden="true">
  <div class="modal-dialog">
    <a href="#" class="modal-close"><i class="ion-ios-close"></i></a>
    <div class="modal-content">
    </div>
  </div>
</div>