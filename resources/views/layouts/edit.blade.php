<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Voyager::setting('title') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">

        @include('layouts/_header')
        
        <div class="editor" id="editor" contenteditable="true">
            @yield('content')
        </div>

        <div class="page-edit-controls">
            <a href="#" class="btn btn-save" id="save"><i class="ion-ios-checkmark-outline"></i></a>
            <a href="#" class="btn btn-cancel" id="cancel"><i class="ion-ios-close"></i></a>
        </div>

                
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script>
    CKEDITOR.on( 'instanceReady', function( ev ) {
        window.onbeforeunload = function(){
            return 'Are you sure you want to leave this page?  Any unsaved changes will be discarded.';
        };
        $('#cancel').click(function(e) {
            e.preventDefault();
            // if content has not changed
            if (!ev.editor.checkDirty()) {
                window.onbeforeunload = function () {}
            }
            // window.location.href = "/admin";
            history.go(-1);
        });
    });
    $('#save').on("click", function(e) {
        e.preventDefault();
        var content = CKEDITOR.instances.editor.getData();  
        $.ajax({
            type: "POST",
            url: '/{{ $page->slug }}/edit',
            data: {
                _token: "{{ csrf_token() }}",
                content: content
            },
            success: function (data) {
                $('body').append('<div class="alert alert-fixed ' + data.message_type + '">' + data.message + '</div>');
            }
        });
    });
    </script>
</body>
</html>
