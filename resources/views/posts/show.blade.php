@if ($post->image)
	<div class="post-image">
		<img src="/storage/{{ $post->image }}" alt="{{ $post->title }}">
	</div>
@endif
<div class="post-info">
	<h1>{{ $post->title }}</h1>
	<div class="post-content">
		{!! $post->content !!}
	</div>
	@if ($post->external_link)
		<a href="{{ $post->external_link }}" class="external-link" target="_blank">Click here to see the article</a>
	@endif
</div>
