<div class="isv-single">
	<div class="isv-images">
		<ul class="light-slider cS-hidden" id="modal-slider">
			@if ($isv->getOriginal('image1'))
				<li>
					<div class="isv-image" style="background-image:url('/storage/{{ $isv->image1 }}');"></div>
					@if ($isv->video_url)
						<div class="video-play-btn" data-video-type="{{ $isv->video_type }}"><i class="ion-ios-play"></i></div>
					@endif
				</li>
			@endif
			@if ($isv->getOriginal('image2'))
				<li><div class="isv-image" style="background-image:url('/storage/{{ $isv->image2 }}');"></div></li>
			@endif
			@if ($isv->getOriginal('image3'))
				<li><div class="isv-image" style="background-image:url('/storage/{{ $isv->image3 }}');"></div></li>
			@endif
		</ul>
	</div>
	@if ($isv->video_url)
		<div class="video-container" id="video-container">
			{!! $isv->video() !!}
		</div>
		<div class="video-stop-btn" data-video-type="{{ $isv->video_type }}"><i class="ion-md-square"></i> Stop Video</div>
	@endif
	<div class="isv-info">
		<h1>{{ $isv->name }}</h1>
		<span class="isv-company-name">By @if($isv->company_link) <a href="{{ $isv->company_link }}" target="_blank"> @endif {{ $isv->company_name }}@if($isv->company_link) </a> @endif</span>
		<div class="isv-content">
			{!! $isv->content !!}
		</div>
		<ul class="isv-categories">
			@foreach ($isv->categories as $category)
				<li>{{ $category->name }}</li>
			@endforeach
		</ul>
	</div>
</div>