@foreach ($isvs as $isv)
	<div class="isv col-md-2-4 col-sm-4 col-xs-6">
		<div class="isv-image">
			<a href="/isvs/{{ $isv->id }}" class="isv-link" ><img src="/storage/{{ $isv->icon }}" alt="{{ $isv->name }}"></a>
		</div>
		<div class="isv-name h4">
			{{ $isv->name }}
		</div>
		<div class="isv-category">
			{{ $isv->categories->implode('name', ', ') }}
		</div>
	</div>
@endforeach