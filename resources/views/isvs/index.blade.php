<!-- ISVs -->
<div class="isvs" id="isvs">
	@include('isvs.items')
</div>
<div id="isv-pagination">{{ $isvs->links() }}</div>
<div id="isv-loading" class="loading"><i class="ion-ios-refresh"></i></div>

<!-- ISV Modal -->
<div class="modal fade" id="isv-modal" tabindex="-1" role="dialog" aria-labelledby="isv-modal" aria-hidden="true">
  <div class="modal-dialog">
	<a href="#" class="modal-close"><i class="ion-ios-close"></i></a>
    <div class="modal-content">
    </div>
  </div>
</div>