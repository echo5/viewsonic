@if ($expo->image)
	<div class="post-image">
		<img src="/storage/{{ $expo->image }}" alt="{{ $expo->name }}">
	</div>
@endif
<div class="post-info">
	<span class="event-date">{{ Carbon\Carbon::parse($expo->start_date)->format('M d, Y') }}</span>
	<h1>{{ $expo->name }}</h1>
	<div class="post-content">
		{!! $expo->content !!}
	</div>
	@if ($expo->external_link)
		<a href="{{ $expo->external_link }}" class="external-link" target="_blank">Click here to see event details</a>
	@endif
</div>
