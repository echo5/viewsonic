<!-- ISV Filters -->
<div class="filters col-md-10 col-md-offset-1" id="isv-filters">
	<div class="row">
		<div class="filter filtername col-md-3 col-xs-6">
			<select name="region">
				<option selected="selected" value="">Region</option>
				@if (count($regionFilter))
					@foreach ($regionFilter as $option)
						<option value="{{ $option->id }}">{{ $option->name }}</option>
					@endforeach
				@endif
			</select>
		</div>
		<div class="filter filtername col-md-3 col-xs-6">
		<select name="segment">
			<option selected="selected" value="">Segment</option>
			@if (count($segmentFilter))
				@foreach ($segmentFilter as $option)
					<option value="{{ $option->id }}">{{ $option->name }}</option>
				@endforeach
			@endif
		</select>
		</div>
		<div class="filter filtername col-md-3 col-xs-6">
		<select name="category">
			<option selected="selected" value="">Category</option>
			@if (count($categoryFilter))
				@foreach ($categoryFilter as $option)
					<option value="{{ $option->id }}">{{ $option->name }}</option>
				@endforeach
			@endif
		</select>
		</div>
		<div class="filter filtername col-md-3 col-xs-6">
		<select name="purpose">
			<option selected="selected" value="">Purpose</option>
			@if (count($purposeFilter))
				@foreach ($purposeFilter as $option)
					<option value="{{ $option->id }}">{{ $option->name }}</option>
				@endforeach
			@endif
		</select>
		</div>
	</div>
</div>