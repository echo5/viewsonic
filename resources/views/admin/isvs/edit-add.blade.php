@extends('voyager::master')

@section('css')
<style>

  .panel .mce-panel{
    border-left-color: #fff;
    border-right-color: #fff;
}

.panel .mce-toolbar, .panel .mce-statusbar {
  padding-left: 20px;
}

.panel .mce-edit-area, .panel .mce-edit-area iframe, .panel .mce-edit-area iframe html{
    padding:0px 10px;
    min-height:350px;
}

.mce-content-body {
    color:#555;
    font-size:14px;
}

.panel.is-fullscreen .mce-statusbar {
    position:absolute;
    bottom:0px;
    width:100%;
}

.panel.is-fullscreen .mce-tinymce{
    height:100%;
}

.panel.is-fullscreen .mce-edit-area, .panel.is-fullscreen .mce-edit-area iframe, .panel.is-fullscreen .mce-edit-area iframe html{
    height: 100%;
    position: absolute;
    width: 95%;
    overflow-y: scroll;
    overflow-x: hidden;
    min-height:100%;

}

</style>
@stop

@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
</h1>
@stop

@section('content')
<div class="page-content container-fluid">

    <form role="form" action="@if(isset($dataTypeContent->id)){{ route($dataType->name . '.update', $dataTypeContent->id) }}@else{{ route($dataType->name . '.store') }}@endif" method="POST" enctype="multipart/form-data">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">

            <div class="col-md-8">

                <!-- ### TITLE ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="voyager-character"></i> Name
                        </h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" name="name" placeholder="Name" value="@if(isset($dataTypeContent->name)){{ $dataTypeContent->name }}@endif">
                    </div>
                </div><!-- .panel -->

                <!-- ### CONTENT ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-book"></i> Content</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                        </div>
                    </div>
                    <textarea class="richTextBox" name="content" style="border:0px;">@if(isset($dataTypeContent->content)){{ $dataTypeContent->content }}@endif</textarea>
                </div><!-- .panel -->

                <!-- ### EXCERPT ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Description <small>Quick description of this app</small></h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                      <textarea class="form-control" name="description">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
                  </div>
              </div><!-- .panel -->

          </div>


          <div class="col-md-4">

                <!-- ### DETAILS ### -->
                <div class="panel panel panel-bordered panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i> ISV Info</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name">Status</label>
                            <select class="form-control" name="status">
                                @foreach (app($dataType->model_name)->statuses() as $key => $label)
                                    <option value="{{ $key }}" @if(isset($dataTypeContent->status) && $dataTypeContent->getOriginal('status') == $key){{ 'selected="selected"' }}@endif>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Company Name</label>
                            <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="@if(isset($dataTypeContent->company_name)){{ $dataTypeContent->company_name }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="name">Company Link</label>
                            <input type="text" class="form-control" name="company_link" placeholder="Company Link" value="@if(isset($dataTypeContent->company_link)){{ $dataTypeContent->company_link }}@endif">
                        </div>
                    </div>
                </div><!-- .panel -->

                <!-- ### DETAILS ### -->
                <div class="panel panel panel-bordered panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i> Filters</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php $filters = isset($dataTypeContent->filters) ? $dataTypeContent->filters->pluck('id')->toArray() : []; ?>
                        @foreach (App\IsvFilter::filterTypes() as $key => $filter)
                            <div class="form-group">
                                <label for="filter_{{ $key }}">{{ $filter }}</label>
                                <select multiple class="form-control select2 filter" name="filter_{{ $key }}">
                                    @foreach (App\IsvFilter::where('filter_type', $key)->get() as $option)
                                        <option value="{{ $option->id }}" @if(is_array($filters) && in_array(intval($option->id), $filters)){{ 'selected="selected"' }}@endif>{{ $option->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endforeach
                        <select multiple name="filters[]" id="filters" style="visibility:hidden; height: 1px;">
                            @foreach(\App\IsvFilter::all() as $option)
                            <option value="{{ $option->id }}" @if(is_array($filters) && in_array(intval($option->id), $filters)){{ 'selected="selected"' }}@endif>{{ $option->name }}</option>
                            @endforeach
                        </select>
                        <script type="text/javascript">
                            var selectFilters = $('select.filter');
                            selectFilters.on('change', function() {
                                var values = [];
                                selectFilters.find('option:selected').each(function(){
                                    values.push(this.value);
                                })
                              $('#filters').val( values );
                            });
                        </script>
                    </div>
                </div><!-- .panel -->


                <!-- ### IMAGE ### -->
                <div class="panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-image"></i> Images</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="icon">Icon</label>
                            <small class="form-text text-muted">This should be a 250x250 image</small>
                            @if(isset($dataTypeContent->icon))
                                <img src="{{ Voyager::image( $dataTypeContent->icon ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="icon">
                        </div>
                        <div class="form-group">
                            <label for="image">Image 1</label>
                            <small class="form-text text-muted">This should be roughly a 475x550 image, but will be cropped and sized to fit responsively based on the screen size.</small>
                            @if(isset($dataTypeContent->image1))
                                <img src="{{ Voyager::image( $dataTypeContent->image1 ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image1">
                        </div>
                        <div class="form-group">
                            <label for="image">Image 2</label>
                            <small class="form-text text-muted">This should be roughly a 475x550 image, but will be cropped and sized to fit responsively based on the screen size.</small>
                            @if(isset($dataTypeContent->image2))
                                <img src="{{ Voyager::image( $dataTypeContent->image2 ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image2">
                        </div>
                        <div class="form-group">
                            <label for="image">Image 3</label>
                            <small class="form-text text-muted">This should be roughly a 475x550 image, but will be cropped and sized to fit responsively based on the screen size.</small>
                            @if(isset($dataTypeContent->image3))
                                <img src="{{ Voyager::image( $dataTypeContent->image3 ) }}" style="width:100%" />
                            @endif
                            <input type="file" name="image3">
                        </div>
                    </div>
                </div><!-- .panel -->

                <!-- ### VIDEO CONTENT ### -->
                <div class="panel panel-bordered panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-search"></i> Video</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name">Video URL</label>
                            <input type="text" class="form-control" name="video_url" placeholder="Video URL" value="@if(isset($dataTypeContent->video_url)){{ $dataTypeContent->video_url }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="name">Video Type</label>
                            <select class="form-control" name="video_type">
                                @foreach (app($dataType->model_name)->videoTypes() as $key => $label)
                                    <option value="{{ $key }}" @if(isset($dataTypeContent->video_type) && $dataTypeContent->video_type == $key){{ 'selected="selected"' }}@endif>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div><!-- .panel -->

            </div><!-- .col-md-4 -->
        </div><!-- .row -->

        <!-- PUT Method if we are editing -->
        @if(isset($dataTypeContent->id))
        <input type="hidden" name="_method" value="PUT">
        @endif
        <!-- PUT Method if we are editing -->

        <!-- CSRF TOKEN -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <button type="submit" class="btn btn-primary pull-right">@if(isset($dataTypeContent->id)){{ 'Update ' . $dataType->display_name_singular }}@else<?= '<i class="icon wb-plus-circle"></i> Create New ' . $dataType->display_name_singular; ?>@endif</button>

    </form>

    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>

</div><!-- .container-fluid -->
@stop

@section('javascript')
<script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
<script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>
@stop