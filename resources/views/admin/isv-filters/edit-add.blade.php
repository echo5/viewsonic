@extends('voyager::master')

@section('css')
<style>

  .panel .mce-panel{
    border-left-color: #fff;
    border-right-color: #fff;
}

.panel .mce-toolbar, .panel .mce-statusbar {
  padding-left: 20px;
}

.panel .mce-edit-area, .panel .mce-edit-area iframe, .panel .mce-edit-area iframe html{
    padding:0px 10px;
    min-height:350px;
}

.mce-content-body {
    color:#555;
    font-size:14px;
}

.panel.is-fullscreen .mce-statusbar {
    position:absolute;
    bottom:0px;
    width:100%;
}

.panel.is-fullscreen .mce-tinymce{
    height:100%;
}

.panel.is-fullscreen .mce-edit-area, .panel.is-fullscreen .mce-edit-area iframe, .panel.is-fullscreen .mce-edit-area iframe html{
    height: 100%;
    position: absolute;
    width: 95%;
    overflow-y: scroll;
    overflow-x: hidden;
    min-height:100%;

}

.panel-maroon .panel-heading {
    background-color: #cc4d9c;
}
.panel-maroon .panel-heading h3 {
    color: #fff;
}

</style>
@stop

@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
</h1>
@stop

@section('content')
<div class="page-content container-fluid">

    <form role="form" action="@if(isset($dataTypeContent->id)){{ route('isv-filters.update', $dataTypeContent->id) }}@else{{ route('isv-filters.store') }}@endif" method="POST" enctype="multipart/form-data">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">

            <div class="col-md-12">

                <!-- ### DETAILS ### -->
                <div class="panel panel panel-bordered">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i> Filter</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" value="@if(isset($dataTypeContent->name)){{ $dataTypeContent->name }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="name">Filter Type</label>
                            <select class="form-control" name="filter_type">
                                @foreach (app($dataType->model_name)->filterTypes() as $key => $label)
                                    <option value="{{ $key }}" @if(isset($dataTypeContent->filter_type) && $dataTypeContent->getOriginal('filter_type') == $key){{ 'selected="selected"' }}@endif>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div><!-- .panel -->

            </div>

        </div><!-- .row -->

        <!-- PUT Method if we are editing -->
        @if(isset($dataTypeContent->id))
        <input type="hidden" name="_method" value="PUT">
        @endif
        <!-- PUT Method if we are editing -->

        <!-- CSRF TOKEN -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <button type="submit" class="btn btn-primary pull-right">@if(isset($dataTypeContent->id)){{ 'Update ' . $dataType->display_name_singular }}@else<?= '<i class="icon wb-plus-circle"></i> Create New ' . $dataType->display_name_singular; ?>@endif</button>

    </form>

    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>

</div><!-- .container-fluid -->
@stop

@section('javascript')
<script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
<script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>
@stop