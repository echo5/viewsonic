@extends('voyager::master')

@section('css')
<style>

  .panel .mce-panel{
    border-left-color: #fff;
    border-right-color: #fff;
}

.panel .mce-toolbar, .panel .mce-statusbar {
  padding-left: 20px;
}

.panel .mce-edit-area, .panel .mce-edit-area iframe, .panel .mce-edit-area iframe html{
    padding:0px 10px;
    min-height:350px;
}

.mce-content-body {
    color:#555;
    font-size:14px;
}

.panel.is-fullscreen .mce-statusbar {
    position:absolute;
    bottom:0px;
    width:100%;
}

.panel.is-fullscreen .mce-tinymce{
    height:100%;
}

.panel.is-fullscreen .mce-edit-area, .panel.is-fullscreen .mce-edit-area iframe, .panel.is-fullscreen .mce-edit-area iframe html{
    height: 100%;
    position: absolute;
    width: 95%;
    overflow-y: scroll;
    overflow-x: hidden;
    min-height:100%;

}

</style>
@stop

@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
</h1>
@stop

@section('content')
<div class="page-content container-fluid">

    <form role="form" action="@if(isset($dataTypeContent->id)){{ route($dataType->name . '.update', $dataTypeContent->id) }}@else{{ route($dataType->name . '.store') }}@endif" method="POST" enctype="multipart/form-data">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">

            <div class="col-md-8">

                <!-- ### TITLE ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="voyager-character"></i> Page Title
                        </h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" name="title" placeholder="Title" value="@if(isset($dataTypeContent->title)){{ $dataTypeContent->title }}@endif">
                    </div>
                </div><!-- .panel -->

                <!-- ### CONTENT ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-book"></i> Edit</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(isset($dataTypeContent->id))
                            <a class="btn btn-success" href="/{{ $dataTypeContent->slug }}/edit">Live Edit</a>
                        @else
                            <p>Please save your page in order to live edit.</p>
                        @endif
                    </div>
                    {{-- <textarea class="richTextBox" name="content" style="border:0px;">@if(isset($dataTypeContent->content)){{ $dataTypeContent->content }}@endif</textarea> --}}
                </div><!-- .panel -->

          </div>


          <div class="col-md-4">

                <!-- ### DETAILS ### -->
                <div class="panel panel panel-bordered panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i> Details</h3>
                        <div class="panel-actions">
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name">Status</label>
                            <select class="form-control" name="status">
                                @foreach (app($dataType->model_name)->statuses() as $key => $label)
                                    <option value="{{ $key }}" @if(isset($dataTypeContent->status) && $dataTypeContent->getOriginal('status') == $key){{ 'selected="selected"' }}@endif>{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Slug</label>
                            <input type="text" class="form-control" name="slug" value="@if(isset($dataTypeContent->slug)){{ $dataTypeContent->slug }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="name">Meta Description</label>
                            <textarea class="form-control" name="meta_description">@if(isset($dataTypeContent->meta_description)){{ $dataTypeContent->meta_description }}@endif</textarea>
                        </div>
                    </div>
                </div><!-- .panel -->

            </div><!-- .col-md-4 -->
        </div><!-- .row -->

        <!-- PUT Method if we are editing -->
        @if(isset($dataTypeContent->id))
        <input type="hidden" name="_method" value="PUT">
        @endif
        <!-- PUT Method if we are editing -->

        <!-- CSRF TOKEN -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <button type="submit" class="btn btn-primary pull-right">@if(isset($dataTypeContent->id)){{ 'Update ' . $dataType->display_name_singular }}@else<?= '<i class="icon wb-plus-circle"></i> Create New ' . $dataType->display_name_singular; ?>@endif</button>

    </form>

    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>

</div><!-- .container-fluid -->
@stop

@section('javascript')
<script src="{{ config('voyager.assets_path') }}/lib/js/tinymce/tinymce.min.js"></script>
<script src="{{ config('voyager.assets_path') }}/js/voyager_tinymce.js"></script>
@stop