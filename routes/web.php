<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

if (!Request::is('admin'))
{
	// Isvs
	Route::get('/isvs', 'IsvsController@index', ['as' => 'isvs']);
	Route::get('/isvs/{id}', 'IsvsController@show', ['as' => 'show_isv']);

	// Posts
	Route::get('/expos/{slug}', 'ExposController@show', ['as' => 'show_expo']);
	Route::get('/news/{slug}', 'PostsController@show', ['as' => 'show_post']);

	// Pages
	if (!Request::is('laravel-filemanager'))
	{
		Route::get('/', 'PagesController@index', ['as' => 'home']);
		Route::get('/{slug}/edit', 'PagesController@edit');
		Route::post('/{slug}/edit', 'PagesController@update');
		Route::get('/{slug}', 'PagesController@show');
	}

}