/**
 * Copyright (c) 2014-2016, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 */

// Note: This automatic widget to dialog window binding (the fact that every field is set up from the widget
// and is committed to the widget) is only possible when the dialog is opened by the Widgets System
// (i.e. the widgetDef.dialog property is set).
// When you are opening the dialog window by yourself, you need to take care of this by yourself too.

CKEDITOR.dialog.add( 'column', function( editor ) {
	return {
		title: 'Edit Column',
		minWidth: 200,
		minHeight: 100,
		contents: [
			{
				id: 'info',
				elements: [
					{
						id: 'column',
						type: 'select',
						label: 'Width',
						items: [
							[ 'One quarter (col-md-3)', 'col-md-3' ],
							[ 'One third (col-md-4)', 'col-md-4' ],
							[ 'One half (col-md-6)', 'col-md-6' ],
							[ 'Two thirds (col-md-8)', 'col-md-8' ],
							[ 'Three quarters (col-md-9)', 'col-md-9' ],
							[ 'Full (col-md-12)', 'col-md-12' ],
						],
						setup: function( widget ) {
							this.setValue( widget.data.column );
						},
						commit: function( widget ) {
							widget.setData( 'column', this.getValue() );
						}
					},
				]
			}
		]
	};
} );